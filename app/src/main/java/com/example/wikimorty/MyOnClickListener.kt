package com.example.wikimorty

import Model.Result

interface MyOnClickListener {
    fun onClick(user: Result)
}