package View

import ViewModel.QuoteCharactersViewModel
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import Model.Result
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.wikimorty.MyOnClickListener
import com.example.wikimorty.R
import com.example.wikimorty.databinding.FragmentRecyclerViewBinding


class RecyclerViewFragment :Fragment(), MyOnClickListener {

    lateinit var binding: FragmentRecyclerViewBinding
    private val viewModel: QuoteCharactersViewModel by activityViewModels()


    private lateinit var userAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
       binding= FragmentRecyclerViewBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.data.observe(viewLifecycleOwner){
            setUpRecyclerView(it)
        }
        if (viewModel.currentPage==1){
            binding.llamaBack.visibility = View.INVISIBLE
            binding.back.visibility = View.INVISIBLE
        }
        binding.next.setOnClickListener {
            binding.llamaBack.visibility = View.VISIBLE
            binding.back.visibility = View.VISIBLE
            viewModel.currentPage += 1
            viewModel.fetchData()
        }
        binding.back.setOnClickListener {
            if (viewModel.currentPage >1){
                viewModel.currentPage -= 1
                viewModel.fetchData()
                if (viewModel.currentPage==1){
                    binding.llamaBack.visibility = View.INVISIBLE
                    binding.back.visibility = View.INVISIBLE
                }
            }
        }
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    if (query != "") {
                        viewModel.name = query
                        viewModel.fetchSearch()
                        binding.back.visibility = View.INVISIBLE
                        binding.next.visibility = View.INVISIBLE
                        binding.llamaBack.visibility = View.INVISIBLE
                        binding.llamaNext.visibility = View.INVISIBLE
                    }
                    else {
                        viewModel.fetchData()
                        if (viewModel.currentPage == 1){
                            binding.llamaNext.visibility= View.VISIBLE
                            binding.next.visibility= View.VISIBLE
                        }
                        else {
                            binding.back.visibility = View.VISIBLE
                            binding.next.visibility= View.VISIBLE
                            binding.llamaBack.visibility = View.VISIBLE
                            binding.llamaNext.visibility= View.VISIBLE
                        }
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    if (newText != "") {
                        viewModel.name = newText
                        viewModel.fetchSearch()
                        binding.back.visibility = View.INVISIBLE
                        binding.next.visibility = View.INVISIBLE
                        binding.llamaBack.visibility = View.INVISIBLE
                        binding.llamaNext.visibility = View.INVISIBLE
                    }
                    else {
                        viewModel.fetchData()
                        if (viewModel.currentPage == 1){
                            binding.llamaNext.visibility= View.VISIBLE
                            binding.next.visibility= View.VISIBLE
                        }
                        else {
                            binding.back.visibility = View.VISIBLE
                            binding.next.visibility= View.VISIBLE
                            binding.llamaBack.visibility = View.VISIBLE
                            binding.llamaNext.visibility= View.VISIBLE
                        }
                    }
                }
                return false
            }
        })

    }
    private fun setUpRecyclerView(listOfUsers: List<Result>){
        userAdapter = UserAdapter(listOfUsers , this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }
    override fun onClick(user: Result) {
        viewModel.setSelectedUser(user)
        findNavController().navigate(R.id.action_recyclerViewFragment2_to_detailFragment)
    }


}