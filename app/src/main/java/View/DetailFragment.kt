package View

import Model.CharacterApplication
import Model.CharacterDAO
import Model.CharacterDatabase
import Model.CharacterEntity
import ViewModel.QuoteCharactersViewModel
import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.room.Query
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.wikimorty.R
import com.example.wikimorty.databinding.FragmentDetailBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailFragment : Fragment() {

    lateinit var binding: FragmentDetailBinding
    private val viewModel: QuoteCharactersViewModel by activityViewModels()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.id.text = "ID: ${viewModel.currentUser?.id}"
        binding.name.text = viewModel.currentUser?.name
        binding.gender.text = "GENDER: ${viewModel.currentUser?.gender!!.uppercase()}"
        binding.status.text = "STATUS: ${viewModel.currentUser?.status!!.uppercase()}"
        Glide.with(requireContext())
            .load(viewModel.currentUser?.image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .circleCrop()
            .into(binding.image)
        checkFav()
        binding.back.setOnClickListener {
            findNavController().navigate(R.id.action_detailFragment_to_recyclerViewFragment2)
            viewModel.fetchData()
        }
        binding.favs.setOnClickListener {
            binding.favs.visibility = View.INVISIBLE
            binding.favsSeelected.visibility = View.VISIBLE
            val newContact = CharacterEntity(name = viewModel.currentUser?.name!!, id = viewModel.currentUser?.id!!, favourite = false, image = viewModel.currentUser?.image!!)
            insertContact(newContact)
        }
        binding.favsSeelected.setOnClickListener {
            binding.favs.visibility = View.VISIBLE
            binding.favsSeelected.visibility = View.INVISIBLE
            val newContact = CharacterEntity(name = viewModel.currentUser?.name!!, id = viewModel.currentUser?.id!!, favourite = false, image = viewModel.currentUser?.image!!)
            deleteCharacter(newContact)
        }
    }
    private fun insertContact(character: CharacterEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            CharacterApplication.database.characterDao().addCharacter(character)
        }
    }
    private fun deleteCharacter(character: CharacterEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            CharacterApplication.database.characterDao().deleteCharacter(character)
        }
    }
    private fun checkFav(){
        CoroutineScope(Dispatchers.IO).launch {
           val result= CharacterApplication.database.characterDao().getFavCharacters(viewModel.currentUser?.id!!)
            if (result==0){
                binding.favs.visibility = View.VISIBLE
                binding.favsSeelected.visibility = View.INVISIBLE
            }
            else {
                binding.favs.visibility = View.INVISIBLE
                binding.favsSeelected.visibility = View.VISIBLE
            }
        }
    }

}