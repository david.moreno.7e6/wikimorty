package View

import Model.CharacterEntity

interface FavouriteOnClickListener {
    fun onClick(character: CharacterEntity)
}