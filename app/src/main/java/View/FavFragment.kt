package View

import Model.CharacterApplication
import Model.CharacterEntity
import ViewModel.QuoteCharactersViewModel
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wikimorty.MyOnClickListener
import com.example.wikimorty.R
import com.example.wikimorty.databinding.FragmentFavBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavFragment : Fragment(), FavouriteOnClickListener {

    lateinit var binding: FragmentFavBinding
    private val viewModel: QuoteCharactersViewModel by activityViewModels()
    private lateinit var favouritesAdapter: FavouritesAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentFavBinding.inflate(layoutInflater)
        return binding.root
    }

    private fun getCharacters(): MutableList<CharacterEntity>{
        val characters = mutableListOf<CharacterEntity>()
        return characters
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favouritesAdapter = FavouritesAdapter(getCharacters(),this)
        linearLayoutManager = LinearLayoutManager(context)

        CoroutineScope(Dispatchers.IO).launch {
            val characters = CharacterApplication.database.characterDao().getAllCharacters()
            withContext(Dispatchers.Main){
                setUpRecyclerFavourites(characters)
            }
        }
    }

    fun setUpRecyclerFavourites(resultList:MutableList<CharacterEntity>){
        val myAdapter = FavouritesAdapter(resultList,this)
        binding.recyclerViewFavourites.apply{
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = myAdapter
        }
    }

    override fun onClick(character: CharacterEntity) {
        val characterResult = viewModel.toResult(character.id)
        viewModel.setSelectedUser(characterResult)
        findNavController().navigate(R.id.action_favFragment_to_detailFragment)
    }
}