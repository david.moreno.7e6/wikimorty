package View

import Model.CharacterEntity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.media.R
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.wikimorty.databinding.ItemUserBinding

class FavouritesAdapter(private val characters: MutableList<CharacterEntity>, private val listener: FavouriteOnClickListener): RecyclerView.Adapter<FavouritesAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemUserBinding.bind(view)
        fun setListener(character: CharacterEntity){
            binding.root.setOnClickListener {
                listener.onClick(character)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouritesAdapter.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(com.example.wikimorty.R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    override fun onBindViewHolder(holder: FavouritesAdapter.ViewHolder, position: Int) {
        val character = characters[position]
        with(holder){
            setListener(character)
            binding.description.text = character.name
            Glide.with(context)
                .load(character.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.image)
        }
    }
}
