package View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.wikimorty.databinding.ActivityLaunchScreenBinding

class LaunchScreen : AppCompatActivity() {

    lateinit var binding: ActivityLaunchScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null){
            supportActionBar?.hide()
        }
        binding= ActivityLaunchScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val handler= Handler(Looper.getMainLooper())
        handler.postDelayed({
            val toMenu= Intent(this, MainActivity::class.java)
            startActivity(toMenu)
            finish()
        }, 3000)

    }
}