package View

import Model.Data
import Model.Result
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.wikimorty.MyOnClickListener
import com.example.wikimorty.R
import com.example.wikimorty.databinding.ItemUserBinding

class UserAdapter(private var users: List<Result>, private val listener: MyOnClickListener):
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemUserBinding.bind(view)
        fun setListener(user: Result){
            binding.root.setOnClickListener {
                listener.onClick(user)
            }
        }
    }
    private lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return users.size
    }
    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = users[position]
        with(holder){
            setListener(user)
            binding.description.text = user.name
            Glide.with(context)
                .load(user.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.image)
        }
    }

}