package ViewModel

import com.example.wikimorty.API.ApiInterface

class Repository {
    val apiInterface = ApiInterface.create()
    suspend fun getCharacters(url: String) = apiInterface.getCharacters(url)
}