package ViewModel

import Model.CharacterApplication
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import Model.Result
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class QuoteCharactersViewModel: ViewModel() {
    private val repository = Repository()
    var data = MutableLiveData<List<Result>>()
    var currentUser: Model.Result? = null
    var currentPage=1
    var name= ""
    init {
        fetchData()
    }
    fun fetchData(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCharacters("character?page=$currentPage")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    data.postValue(response.body()!!.results)
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun fetchSearch(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCharacters("character/?name=$name")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    data.postValue(response.body()!!.results)
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun setSelectedUser(user: Model.Result?){
        currentUser = user
    }

    fun toResult(id:Int):Model.Result?{
        var character: Result? = null
        var i = 0
        while (i in data.value!!.indices&&character==null){
            if(id == data.value!![i].id) character = data.value!![i]
            i++
        }
        return character
    }

}