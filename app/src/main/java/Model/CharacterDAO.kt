package Model

import androidx.room.*

@Dao
interface CharacterDAO {
    @Query("SELECT * FROM CharacterEntity")
    fun getAllCharacters(): MutableList<CharacterEntity>

    @Query("SELECT count(*) FROM CharacterEntity WHERE id = :id")
    fun getFavCharacters(id: Int): Int

    @Insert
    fun addCharacter(characterEntity: CharacterEntity)

    @Update
    fun updateCharacter(characterEntity: CharacterEntity)

    @Delete
    fun deleteCharacter(characterEntity: CharacterEntity)
}