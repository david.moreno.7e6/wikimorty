package Model

import androidx.room.Entity
import androidx.room.PrimaryKey



@Entity(tableName = "CharacterEntity")
data class CharacterEntity(
    @PrimaryKey var id: Int,
    var name:String,
    var image: String,
    var favourite: Boolean)
