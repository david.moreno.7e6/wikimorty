package Model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(CharacterEntity::class), version = 1)
abstract class CharacterDatabase: RoomDatabase() {
    abstract fun characterDao(): CharacterDAO
}