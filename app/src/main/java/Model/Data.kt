package Model

data class Data(
    val info: Info,
    val results: List<Result>
)